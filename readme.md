# Django polls test
Based on old 1.11 tutorial! Don't use on Django 2 projects..
https://docs.djangoproject.com/en/1.11/intro/tutorial01/


## Commands memo
Start server on custom port: `python manage.py runserver 9090`

Make migrations: `python manage.py makemigrations polls`

Show migrations: `python manage.py showmigrations polls`

Migrate: `python manage.py migrate`

Django shell: `python manage.py shell`